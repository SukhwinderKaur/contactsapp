package com.example.saini.contactsapp;

import android.widget.Filter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saini on 11/4/2017.
 */

public class CustomFilter extends Filter
{
    public List<Contact> contactsList=new ArrayList<Contact> (  );

    //create new COntact List from contactsList for searching
    List<Contact> filterList=contactsList;
    @Override
    //method for filtering data from list , constraint is the query typed by user
    protected FilterResults performFiltering(CharSequence constraint) {

        //filterResult Default class used for holding filtered results
        FilterResults filterResults=new FilterResults ();

        //check if query is not null and length of query is greater than zero
        if(constraint!= null && constraint.length ()>0)
        {
            //constarint to upper case
            constraint=constraint.toString ().toUpperCase ();

            ArrayList<Contact> filters=new ArrayList<Contact> (  );
            //get Specific Items
            for(int i=0;i<filterList.size ();i++)
            {
                if(filterList.get(i).getfName ().toUpperCase ().contains ( constraint ))
                {
                    Contact c=new Contact ( filterList.get(i).getfName (),filterList.get(i).getlName ()
                            ,filterList.get(i).getPhone (),filterList.get(i).getEmail (),filterList.get(i).getAddress (),
                            filterList.get(i).getImgUri ());
                    filters.add ( c );
                }
            }
            filterResults.count=filters.size ();
            filterResults.values=filters;
        }
        else
        {
            filterResults.count=filterList.size ();
            filterResults.values=filterList;
        }
        return filterResults ;
    }

    @Override// show results of search using filterResult Class and itz values
    protected void publishResults(CharSequence constraint, FilterResults filterResults) {
        contactsList=(List<Contact>)filterResults.values;
            }
}
