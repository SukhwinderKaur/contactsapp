package com.example.saini.contactsapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by saini on 10/30/2017.
 */
//database helper class for Our application
public class DatabaseHepler extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;

    //declaring database name , table name and fields
    public static final String DATABASE_NAME = "ContactList.db",
            TABLE_CONTACT = "contact",
            COLUMN_ID = "id",
            COLUMN_FIRSTNAME = "FirstName",
            COLUMN_LASTNAME = "lastname",
            COLUMN_EMAIL = "email",
            COLUMN_ADDRESS = "address",
            COLUMN_IMAGEURI = "imageuri";
    public static final String COLUMN_PHONE1 = "phone";

    //call to super class constructor for creating database
    public DatabaseHepler(Context context) {
        super ( context, DATABASE_NAME, null, DATABASE_VERSION );
    }

    @Override//creation of table
    public void onCreate(SQLiteDatabase db) {
        db.execSQL ( "Create table " + TABLE_CONTACT + "( "
                + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
                + COLUMN_FIRSTNAME + " text ,"
                + COLUMN_LASTNAME + " text ,"
                + COLUMN_PHONE1 + " text ,"
                + COLUMN_EMAIL + " text ,"
                + COLUMN_ADDRESS + " text ,"
                + COLUMN_IMAGEURI + " text )" );
    }

    @Override//drop table if already exists and create new
    public void onUpgrade(SQLiteDatabase db, int oldVer, int newVer)
    {
        db.execSQL ( "Drop Table if exists " + TABLE_CONTACT );
        onCreate ( db );
    }


    //function for creating contact in database with object of Contact Class
    public boolean createContact(Contact contact)
    {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase ( );

        //COntentValues Creates an empty set of values using the default initial size
        ContentValues values = new ContentValues ( );
        values.putNull ( COLUMN_ID );       //put null if autoincrement field is used
        values.put ( COLUMN_FIRSTNAME, contact.getfName ( ) );
        values.put ( COLUMN_LASTNAME, contact.getlName ( ) );
        values.put ( COLUMN_PHONE1, contact.getPhone ( ) );
        values.put ( COLUMN_EMAIL, contact.getEmail ( ) );
        values.put ( COLUMN_ADDRESS, contact.getAddress ( ) );
        values.put ( COLUMN_IMAGEURI, contact.getImgUri ( ).toString ( ) );

        //insert statement for inserting data to table from Content values
        long result = sqLiteDatabase.insert ( TABLE_CONTACT, null, values );

        //if result is -1 that means insert is failed so return false to calling method otherwise return true
        if (result == -1)
        {
            return false;
        }
        else
            {
            return true;
        }
    }
        //function for deleting contact by id getter from contact class
    public boolean deleteContact(Contact contact)
    {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase ( );
        long res=sqLiteDatabase.delete ( TABLE_CONTACT, COLUMN_ID + " =?", new String[]{String.valueOf (contact.getId () )} );
        sqLiteDatabase.close ( );
        if (res == -1)
        {
            return false;
        } else
            {
            return true;
        }
    }

    //function to get count of contacts
    public int getContactCount()
    {
        int count = 0;
        SQLiteDatabase sqLiteDatabase = getReadableDatabase ( );
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + TABLE_CONTACT, null);
        if (cursor != null)
            count = cursor.getCount ( );

        cursor.close ( );
        sqLiteDatabase.close ( );
        return count;
    }

    //function to update a contact using content values
    public int updateContact(Contact contact)
    {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase ( );
        ContentValues values = new ContentValues ( );

        values.put ( COLUMN_FIRSTNAME, contact.getfName ( ) );
        values.put ( COLUMN_LASTNAME, contact.getlName ( ) );
        values.put ( COLUMN_PHONE1, contact.getPhone ( ) );
        values.put ( COLUMN_EMAIL, contact.getEmail ( ) );
        values.put ( COLUMN_ADDRESS, contact.getAddress ( ) );
        values.put ( COLUMN_IMAGEURI, contact.getImgUri ( ).toString ( ) );
        int rowAffected = sqLiteDatabase.update ( TABLE_CONTACT, values, COLUMN_ID + " =? ",
                new String[]{String.valueOf ( contact.getId ( ) )} );
        sqLiteDatabase.close ( );
        return rowAffected;
    }


    //getting list of all the contacts for Displaying on list View on TAb 2 of our app
    public List<Contact> getAllContacts()
    {
        List<Contact> dbcontacts = new ArrayList<Contact> ( );
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase ( );
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + TABLE_CONTACT, null);

        if (cursor.moveToFirst ( )) {
            do {
                dbcontacts.add ( new Contact ( cursor.getString ( 1 ),
                        cursor.getString ( 2 ), cursor.getString ( 3 ), cursor.getString ( 4 ),
                        cursor.getString ( 5 ), Uri.parse ( cursor.getString ( 6 ) ) ) );
            } while (cursor.moveToNext ( ));

        }
        cursor.close ( );
        sqLiteDatabase.close ( );
        return dbcontacts;
    }
}