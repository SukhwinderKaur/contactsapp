package com.example.saini.contactsapp;

import android.net.Uri;

public class Contact
{
    //id field
    private int id;
    //fields for name phone, email addresss
    private String fName,lName,phone,email,address;
    //field Uri for storing image path
    private Uri imgUri;

    //constructor with all fields
    public Contact(int id, String fName, String lName, String phone, String email, String address, Uri imgUri) {
        this.id = id;
        this.fName = fName;
        this.lName = lName;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.imgUri = imgUri;
    }

    //constructor without AutoIncrement ID field
    public Contact( String fName, String lName, String phone, String email, String address, Uri imgUri) {
        this.fName = fName;
        this.lName = lName;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.imgUri = imgUri;
    }

    //Setters for all fields
    public void setId(int id)
    {
        this.id = id;
    }

    public void setfName(String fName)
    {
        this.fName = fName;
    }

    public void setlName(String lName)
    {
        this.lName = lName;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public void setImgUri(Uri imgUri)
    {
        this.imgUri = imgUri;
    }

    public Uri getImgUri()
    {
        return imgUri;
    }

    //getters for all the fields
    public int getId()
    {
        return id;
    }

    public String getfName()
    {
        return fName;
    }

    public String getlName()
    {
        return lName;
    }


    public String getPhone()
    {
        return phone;
    }

    public String getEmail()
    {
        return email;
    }


    public String getAddress()
    {
        return address;
    }

}
