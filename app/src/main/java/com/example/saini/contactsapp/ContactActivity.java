package com.example.saini.contactsapp;
/*importing classes for usage */
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static android.widget.TabHost.*;

/*MainActivity for contact App which has two tabs , one for viewing contact list and other is for adding new contact*/

public class ContactActivity extends AppCompatActivity
{
    private static final int EDIT=0,DELETE=1;                               //final fields used for selection of case Edit and delete
    EditText txtFirstName,txtLastName,txtPhone,txtEmail,txtAddress;                        //Editable Text fields to enter COntact Details
    Button btnSaveContact,moreContact;                                                  //Action Button For saving Contact
    ImageView imageHolder;                                                 //Image view to view Image of conatact
    public List<Contact> contactsListArray = new ArrayList<Contact>();                   //ArrayList Containing contacts
    public ListView contactListView;                                           //list view for tab 2 to view all contacts
    private android.widget.EditText[] phonesList;                           //Phone CUstom EditText For adding at Runtime
    private int iPhoneCount = -1;                                           //count for saving number of contacts
    private LinearLayout mLayout;                                           //child layout to show multiple txtPhone Text Box
    Uri imgUri=Uri.parse("android.resourse://com.example.saini.contactsapp/drawable/no_user.png");// Image path for storing user image
    DatabaseHepler dbHelper;                                    //Database Class to access database
    SearchView searchView;
    int selectedContact;                                                        //index for Long click on List View of contacts
    ArrayAdapter contactAdapter;                                                //Array Adapter for list
    boolean isEditMode=false;                                                         //boolean for  edit mode is enabled or not

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.appcontact);                        //set layout for Main Activity 

        //Getting all elements by Id
        txtFirstName = (EditText) findViewById(R.id.txtFirstName);
        txtLastName = (EditText) findViewById(R.id.txtLastName);
        txtPhone = (EditText) findViewById(R.id.txtPhone);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtAddress = (EditText) findViewById(R.id.txtAddress);
        btnSaveContact = (Button) findViewById(R.id.btnSave);
        moreContact = (Button) findViewById(R.id.morePhone);
        imageHolder=(ImageView)findViewById(R.id.imageHolder);
        contactListView=(ListView)findViewById(R.id.viewList);
        
        //view used for searching from contact List View
        searchView=(SearchView)findViewById ( R.id.searchView );
        
        //creating object of database hepler class
        dbHelper=new DatabaseHepler(getApplicationContext());
        
        phonesList = new android.widget.EditText[8];
        moreContact.setOnClickListener(onClick());
        mLayout = (LinearLayout) findViewById(R.id.child);
        //set contact view for opening long Click menu
        registerForContextMenu ( contactListView );

        //Tabhost used for showing two tabs one for add contact and other for View COntact
        final TabHost tabs=(TabHost)findViewById(R.id.allTabs) ;
        tabs.setup();

        //Add Contact Tab
        TabHost.TabSpec tabSpec=tabs.newTabSpec("Create");
        tabSpec .setContent(R.id.addTab);
        tabSpec.setIndicator("Creator");
        tabs.addTab(tabSpec);

        //View All COntact Tab
        tabSpec=tabs.newTabSpec("View All");
        tabSpec .setContent(R.id.viewTab);
        tabSpec.setIndicator("List");
        tabs.addTab(tabSpec);

        //Listener for enabling save button when user enter some text in first name box otherwise the button is disabled
        txtFirstName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                btnSaveContact.setEnabled(String.valueOf(txtFirstName.getText()).trim().length()>0);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        //listener for Save Button
        btnSaveContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Creating Object OF COntact Class and passing data by getting it from text boxex
                Contact contactObj=new Contact(String.valueOf(txtFirstName.getText()),
                        String.valueOf(txtLastName.getText()),
                        String.valueOf ( txtPhone.getText () )+" , "+ ( collectPhones ( mLayout )),
                        String.valueOf(txtEmail.getText()),
                        String.valueOf(txtAddress.getText()),imgUri);
                if(isEditMode){
                                       Contact newContactDetail = contactsListArray.get(selectedContact);
                                        newContactDetail.setfName ((String.valueOf(txtFirstName.getText())));
                                        newContactDetail.setlName (String.valueOf(txtFirstName.getText()));
                                        newContactDetail.setPhone (String.valueOf(txtPhone.getText()));
                                        newContactDetail.setEmail(String.valueOf(txtEmail.getText()));
                                        newContactDetail.setAddress(String.valueOf(txtAddress.getText()));
                                        newContactDetail.setImgUri ( imgUri );
                                        dbHelper.updateContact(newContactDetail);
                                        isEditMode = false;
                                        btnSaveContact.setText("ADD CONTACT");
                                        contactAdapter.notifyDataSetChanged();
                                        resetAddContactPanel();
                                        tabs.setCurrentTab(1);
                                   }
                //call to Create Contact Function for entering data to database
                dbHelper.createContact ( contactObj );

                //code displays name of created contact if the contact is saved successfully
                if(dbHelper.createContact ( contactObj )== true) {
                    Toast.makeText ( getApplicationContext ( ), txtFirstName.getText ( ).toString ( ) + " Contact Created", Toast.LENGTH_SHORT ).show ( );
                }
                //adding contact to list View on 2nd tab
                contactsListArray.add ( contactObj );
                contactAdapter.notifyDataSetChanged ();
            }
        });


        //listener for Image view On create Contact Tab, it opens gallery for chossing the contact
        imageHolder.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent();
                // Show only images, no videos or anything else by setType()
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);

                ////below line will Always show the chooser (if there are multiple options available)
                //parameter 1 in startActivityForResult  is the request code defined as an instance variable.
                startActivityForResult(Intent.createChooser(intent,"Select Contact Image"),1);
            }
        });

        //listener on Contact List View for setting Long Click item position
        contactListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            //Callback method to be invoked when an item in view has been clicked and held and sets position of click.
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                selectedContact = position;
                return false;
            }
        });

        //Sets a listener for user actions within the SearchView.
        searchView.setOnQueryTextListener ( new SearchView.OnQueryTextListener ( ) {
            @Override
            //Called when the user submits the query.
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            //Called when the query text is changed by the user.
            public boolean onQueryTextChange(String query) {
                contactAdapter.getFilter ().filter ( query );
                return false;
            }
        } );
        //class getContactsCont in database class if it is not equal to zero then fill list view with all COntacts from database
        if(dbHelper.getContactCount () !=0)
        {
            contactsListArray.addAll ( dbHelper.getAllContacts () );
        }
        //method used for displaying contact list
        populateContactList();

    }
    //add listener to Create custom Views for Adding multiple Phone Numbers
    private View.OnClickListener onClick() {
        return new View.OnClickListener () {

            @Override
            public void onClick(View v) {
                //call to create new Child layout for entering multiple phone numbers
                mLayout.addView (createNewTextView());
            }
        };
    }

    //this Function create new Edit Text Field for Phone each time a button is clicked
    private EditText createNewTextView() {
        iPhoneCount++;

        //setting layout parameters
        final LinearLayout.LayoutParams lparams = new LinearLayout.LayoutParams ( LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        phonesList[iPhoneCount] = new EditText (this);

        //generate id for custom Edit Text
        phonesList[iPhoneCount].setId ( View.generateViewId ());

        //set layout for Custom Phone field
        phonesList[iPhoneCount].setLayoutParams ( lparams );

        return phonesList[iPhoneCount];
    }


    //this function returns String for multiple Phones added by user
    public String collectPhones(android.view.View view)
    {
        String sPhones = "";
        for (int i = 0; i < phonesList.length; i++)
            if (phonesList[i] != null && phonesList[i].getText ().length () > 0)
                sPhones += phonesList[i].getText ().toString ().trim () +",";

        return sPhones.substring ( 0, sPhones.length () - 1 );
    }

    //Creating menu for Long Click on Item in list
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu ( menu, v, menuInfo );

        menu.setHeaderIcon ( R.drawable.edit );
        menu.setHeaderTitle ( "Contact Options" );
        menu.add ( Menu.NONE,EDIT,Menu.NONE,"Edit Contact" );
        menu.add ( Menu.NONE,DELETE,Menu.NONE,"Delete Contact" );
    }

    //creation of Function performed on long click item selection
    @Override
    public boolean onContextItemSelected(MenuItem item) {

        switch (item.getItemId ())
        {
            case EDIT:
                //call to method for enabling Edit mode in Create Contact Tab.
                enableEditMode(contactsListArray.get(selectedContact));
                break;
            case DELETE:

                //delete Data from list and database using deleteCOntact Function in database hepler class
                dbHelper.deleteContact ( contactsListArray.get(selectedContact));
                if(dbHelper.deleteContact (contactsListArray.get(selectedContact)))
                {
                    Toast.makeText ( getApplicationContext (),"deleted",Toast.LENGTH_SHORT ).show ();
                }
                contactsListArray.remove ( selectedContact );
                contactAdapter.notifyDataSetChanged ();
                break;
        }
        return super.onContextItemSelected ( item );
    }

    /*once the selection of image has been made, we’ll show up the image in the Activity user interface,
     using an ImageView. For this, we’ll have to override onActivityResult():*/
    public void onActivityResult(int reqCode, int resCode,Intent data) {
        if (resCode == RESULT_OK) {
            if (reqCode == 1) {
                //get data from intent for image
                imgUri = (Uri) data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap ( getContentResolver ( ), imgUri );
                    imageHolder.setImageBitmap ( bitmap );
                }
                catch (Exception e)
                {
                    e.printStackTrace ();
                }
            }
        }
    }

    //definition of polulate Contact Lsit
    public void populateContactList()
    {
        // Create Object of ContactListAdapter Subclass
        contactAdapter=new ContactListAdapter();
        //Set Adapter
        contactListView.setAdapter(contactAdapter);
    }

    private void  enableEditMode(Contact contactDetail)
    {
                //get tab
                  TabHost tabHost = (TabHost)findViewById(R.id.allTabs);
                  tabHost.setCurrentTab(0);
                  txtFirstName.setText(contactDetail.getfName ());
                  txtLastName.setText(contactDetail.getlName ());
                  txtPhone.setText(contactDetail.getPhone());
                  txtEmail.setText(contactDetail.getEmail());
                  txtAddress.setText(contactDetail.getAddress());
                  imgUri = contactDetail.getImgUri ();
                  imageHolder.setImageURI(imgUri);
                  Button edit = (Button)findViewById(R.id.btnSave);
                  edit.setText("Update");
                  isEditMode = true;
    }

    //
    private void resetAddContactPanel()
    {
               txtFirstName.setText("");
               txtLastName.setText("");
               txtPhone.setText("");
               txtEmail.setText("");
               txtAddress.setText("");
               imageHolder.setImageURI(imgUri);
    }

        // Sub Class for tab 2 for viewing contacts
        private class ContactListAdapter extends ArrayAdapter<Contact> implements Filterable {
        CustomFilter filter;
            public ContactListAdapter() {
                super (ContactActivity.this, R.layout.listviewitem, contactsListArray);
            }

            public int getCount() {
                return contactsListArray.size();
            }

            @Override
            public Contact getItem(int position) {
                return contactsListArray.get(position);
            }

            @Override
            public long getItemId(int position) {
                return contactsListArray.indexOf ( getItem ( position ) );
            }
            @Override
            public View getView(int position, View view, ViewGroup parent) {
                if (view == null)
                    view = getLayoutInflater().inflate(R.layout.listviewitem, parent, false);

                Contact currentContact = contactsListArray.get(position);

                TextView fName = (TextView) view.findViewById(R.id.fName);
                fName.setText(contactsListArray.get(position).getfName()+" "+contactsListArray.get(position).getlName ());

                TextView cPhone = (TextView) view.findViewById(R.id.cPhone);
                cPhone.setText(contactsListArray.get(position).getPhone());

                TextView txtEmail=(TextView)view.findViewById ( R.id.email );
                txtEmail.setText ( contactsListArray.get(position).getEmail () );

                TextView txtAddress=(TextView)view.findViewById ( R.id.adress );
                txtAddress.setText ( contactsListArray.get(position).getAddress () );

                ImageView userImg = (ImageView) view.findViewById(R.id.userImg);
                userImg.setImageURI ( contactsListArray.get(position).getImgUri ( ) );
                return view;
            }
            public Filter getFilter() {
                if(filter== null)
                {
                    //filter for search in list View
                    filter= new CustomFilter ();
                }
                return filter;
            }

        }

    }
